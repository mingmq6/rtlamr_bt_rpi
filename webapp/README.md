# Google Cloud and Google Map Application

## Node.js Setup Using Cloud Pub/Sub
This section is copied from Google online document.

![Cloud SQL Build Status][ci-badge-cloudsql] ![Datastore Build Status][ci-badge-datastore] ![MongoDB Build Status][ci-badge-mongodb]

[ci-badge-datastore]: https://storage.googleapis.com/nodejs-getting-started-tests-badges/6-datastore.svg
[ci-badge-cloudsql]: https://storage.googleapis.com/nodejs-getting-started-tests-badges/6-cloudsql.svg
[ci-badge-mongodb]: https://storage.googleapis.com/nodejs-getting-started-tests-badges/6-mongodb.svg

This folder contains the sample code for the [Using Cloud Pub/Sub][step-6]
tutorial. Please refer to the tutorial for instructions on configuring, running,
and deploying this sample.

[step-6]: https://cloud.google.com/nodejs/getting-started/using-pub-sub

## Simple instructions

1.  Install [Node.js](https://nodejs.org/en/).

    * Optional: Install [Yarn](https://yarnpkg.com/).

1.  Install [git](https://git-scm.com/).
1.  Create a [Google Cloud Platform project](https://console.cloud.google.com).
1.  Install the [Google Cloud SDK](https://cloud.google.com/sdk/).

    * After downloading the SDK, initialize it:

            gcloud init

1.  Acquire local credentials for authenticating with Google Cloud Platform
    services:

        gcloud beta auth application-default login

1.  Clone the repository:

        git clone https://github.com/GoogleCloudPlatform/nodejs-getting-started.git

1.  Change directory:

        cd nodejs-getting-started/5-logging

1.  Create a `config.json` file (copied from the `config-default.json` file):

        cp config-default.json config.json

    * Set `GCLOUD_PROJECT` in `config.json` to your Google Cloud Platform
      project ID.
    * Set `DATA_BACKEND` in `config.json` to one of `"datastore"`, `"cloudsql"`,
      or `"mongodb"`.
    * Set `CLOUD_BUCKET` in `config.json` to the name of your Google Cloud
      Storage bucket.
    * Set `OAUTH2_CLIENT_ID` in `config.json`.
    * Set `OAUTH2_CLIENT_SECRET` in `config.json`.
    * Set `TOPIC_NAME` in `config.json`.
    * Set `SUBSCRIPTION_NAME` in `config.json`.

1.  Install dependencies using NPM or Yarn:

    * Using NPM:

            npm install

    * Using Yarn:

            yarn install

1.  Configure the backing store:

    * If `DATA_BACKEND` is set to `"cloudsql"`:

        1.  Create a Cloud SQL instance, and download and start the Cloud SQL
            proxy:

            Instructions for doing so: https://cloud.google.com/nodejs/getting-started/using-cloud-sql#creating_a_cloud_sql_instance
        1.  Set `MYSQL_USER` in `config.json`, e.g. `"my-cloudsql-username"`.
        1.  Set `MYSQL_PASSWORD` in `config.json`, e.g. `"my-cloudsql-password"`.
        1.  Set `INSTANCE_CONNECTION_NAME` in `config.json`, e.g. `"YOUR_PROJECT_ID:YOUR_REGION:YOUR_INSTANCE_ID"`.
        1.  Run the script to setup the table:

            * Using NPM:

                    npm run init-cloudsql

            * Using Yarn:

                    yarn run init-cloudsql

    * If `DATA_BACKEND` is set to `"mongodb"`:

        1.  Set `MONGO_URL` in `config.json`, e.g. `"mongodb://username:password@123.45.67.890:27017"`.

1.  Start the app using NPM or Yarn:

    * Using NPM:

            npm start

    * Using Yarn:

            yarn start

1.  View the app at [http://localhost:8080](http://localhost:8080).

1.  Stop the app by pressing `Ctrl+C`.

1.  Deploy the app:

        gcloud app deploy

1.  View the deployed app at [https://YOUR_PROJECT_ID.appspot.com](https://YOUR_PROJECT_ID.appspot.com).

## Cloud Pub/Sub node.js hacking for utility vehicle tracking application

  The Google bookshelf node.js application is modified here to lively receive GPS tracking data of utility vehicle streamed from Helium channel through cloud pub/sub. Each received data packet will trigger a server-sent event to user's browser that plots the utility truck path as a polyline on Google Map using Google Map API.

### app.js - change view engine from pug to ejs 

  The original node.js app uses pug template engine. Because the way of using HTML in this utilityApp, I commented out pug and change it to ejs template. The modified code in [*app.js*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/app.js) reads as:
```
app.set('view engine', 'ejs');
//app.set('view engine', 'pug');
app.engine('html', require('ejs').renderFile);
```

### Three-step approach to achieve the final goal of rendeing live GPS data on Google Map

#### Step 1 - Test polyline plotting on Google Map on localhost

I created the following functions in [*books/crud.js*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/books/crud.js) to generate new set of GPS coordinates every 500mS to be plotted on Google Map.

```
function test(req, res) {
    if (req.headers.accept && req.headers.accept == 'text/event-stream') {
      res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
      });
        var lat = 39.530895;
        var lng = -119.814972;
        setInterval(function() {
            var id = (new Date()).toLocaleTimeString();
            res.write('id: ' + id + '\n');
            lat = lat + 0.001;
            lng = lng + 0.001;
            res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
	    }, 500);
        res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
    } else {
        res.writeHead(200, {
            'Content-Type': 'text/html',
        });
        res.write(fs.readFileSync('./views/books/assert_track.html'));
        res.end();
    }
}

/**
 * GET /gps
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/gps', (req, res, next) => {
  getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
    if (err) {
      next(err);
      return;
    }
    test(req, res);
    //subscribe(req, res);
  });
});
```

Now starts the Node.js app with `npm start`, 

```
pi@raspberrypi:~/proj/amr_fleet_mon/webapp $ npm start

> nodejs-getting-started@1.0.0 start /home/pi/proj/amr_fleet_mon/webapp
> node ${SCRIPT:-app.js}

App listening on port 8080
```

Then you enter "http://localhost:8080/books/gps" on a browser, [*views/books/assert_track.html*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/views/books/assert_track.html) will be rendered and you should see the movement of geo marker on the Google Map

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/bde0c1deb2d0db2b1fb3152c4fd23b14f46b41c9/raspberrypi/images/test_polyline_google_map.png)

#### Step 2 - Test subscribe function and stream update with server-sent events

I created the following functions in [*books/crud.js*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/books/crud.js) to again generate new set of GPS coordinates every 500mS. But instead of plotting these data directly on Google Map, I used the Google Cloud Pub/Sub emulator to publish the data to the topic name "amr-gps". Both data generation and publishing are accomplished by the `test_publish()` function. `subscribe(req, res)` function listens for messages then performs stream updates with server-sent events.

Before testing, make sure TEST is set to true, comment out `test(req, res)` and uncomment `subscribe(req, res)` in `router.get('/gps', (req, res, next)`.

```
const TEST = true;

function subscribe (req, res) {
  if (req.headers.accept && req.headers.accept == 'text/event-stream') {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
  // Subscribe to Cloud Pub/Sub and receive messages to process books.
  // The subscription will continue to listen for messages until the process
  // is killed.
  // [START subscribe]
  const unsubscribeFn = background.subscribe((err, data) => {
    // Any errors received are considered fatal.
    if (err) {
      throw err;
    }
    if (req.url == '/gps') {
      var str_array;
      var datatype;
      var lat, lng;
      logging.info('Received data ' + JSON.stringify(data));
      if ("datatype" in data) {
        datatype = data.datatype
        if (datatype == GPS_DATATYPE) {
          lat = data.lat;
          lng = data.lng;
        }
      } else {
          var dataObj = JSON.parse(JSON.stringify(data));
          if (dataObj.type == "Buffer") {
            var mystr = String.fromCharCode.apply(null, dataObj.data);
            logging.info(`Received data string ${mystr}`);
            str_array = mystr.split(',');
            datatype = parseInt(str_array[0]);
            if (datatype == GPS_DATATYPE) {
              if (TEST) {
                lat = parseFloat(str_array[1]);
                lng = parseFloat(str_array[2]);
              } else {
                // id will be used in the future for tracking 
                // multiple utility trucks
                var id = str_array[1]
                logging.info('Device cpuserial ' + id);
                lat = parseFloat(str_array[2]);
                lng = parseFloat(str_array[3]);
              }
            }
          }
      }

      if (datatype == GPS_DATATYPE) {
        logging.info(`Received request to process (${lat}, ${lng})`);
        //processBook(data.bookId);
        var id = (new Date()).toLocaleTimeString();
        res.write('id: ' + id + '\n');
        res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
      } else if (datatype == METER_DATATYPE) {
        logging.info(`Received request to process ${data}`);
      } else {
        logging.warn('Unknown data type');
      }
    } else {
      logging.warn('Unknown request');
    }
  });
  // [END subscribe]
  return unsubscribeFn;
  } else {
    res.writeHead(200, {
        'Content-Type': 'text/html',
    });
    res.write(fs.readFileSync('./views/books/assert_track.html'));
    res.end();
  }
}

function test_publish() {
  const publishFn = background.getTopic((err, topic) => {
    if (err) {
      logging.error('Error occurred while getting pubsub topic', err);
      return;
    }

    const publisher = topic.publisher();
    var lat = 39.530895;
    var lng = -119.814972;
    var data = {
      datatype: GPS_DATATYPE,
      lat: lat,
      lng: lng
    }
    var mystr = data.datatype.toString() + ',' + data.lat.toString() + ',' + data.lng.toString();
    setInterval(function() {
      lat = lat + 0.001;
      lng = lng + 0.001;
      data = {
        datatype: GPS_DATATYPE,
        lat: lat,
        lng: lng
      }
      mystr = data.datatype.toString() + ',' + data.lat.toString() + ',' + data.lng.toString();
      //publisher.publish(Buffer.from(JSON.stringify(data)), (err) => {
      publisher.publish(Buffer.from(mystr), (err) => {
        if (err) {
          logging.error('Error occurred while publishing GPS data', err);
        } else {
          logging.info('GPS queued for background processing');
        }
      });
    }, 500);
    //publisher.publish(Buffer.from(JSON.stringify(data)), (err) => {
    publisher.publish(Buffer.from(mystr), (err) => {
      if (err) {
        logging.error('Error occurred while publishing GPS data', err);
      } else {
        logging.info(`GPS queued for background processing`);
      }
    });
  });
}

/**
 * GET /books
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/', (req, res, next) => {
  getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
    if (err) {
      next(err);
      return;
    }
    res.render('books/location.html');
    if (TEST) {
      test_publish();
    }
    /*res.render('books/list.pug', {
      books: entities,
      nextPageToken: cursor
    });*/
  });
});

/**
 * GET /gps
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/gps', (req, res, next) => {
  getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
    if (err) {
      next(err);
      return;
    }
    //test(req, res);
    subscribe(req, res);
  });
});

```
  
Before issuing `npm start`, open another terminal and type `gcloud beta emulators pubsub start` to start pub/sub emulator.

```
pi@raspberrypi:~ $ gcloud beta emulators pubsub start
Executing: /usr/lib/google-cloud-sdk/platform/pubsub-emulator/bin/cloud-pubsub-emulator --host=localhost --port=8085
[pubsub] This is the Google Pub/Sub fake.
[pubsub] Implementation may be incomplete or differ from the real system.
[pubsub] Jun 10, 2018 3:25:16 PM com.google.cloud.pubsub.testing.v1.Main main
[pubsub] INFO: IAM integration is disabled. IAM policy methods and ACL checks are not supported
[pubsub] Jun 10, 2018 3:25:18 PM io.gapi.emulators.netty.NettyUtil applyJava7LongHostnameWorkaround
[pubsub] INFO: Applied Java 7 long hostname workaround.
[pubsub] Jun 10, 2018 3:25:19 PM com.google.cloud.pubsub.testing.v1.Main main
[pubsub] INFO: Server started, listening on 8085
```

After app starts with `npm start`, first enter "http://localhost:8080" on your browser. This will start data generation and message publishing. Then enter "http://localhost:8080/books/gps" on your browser, which will plot the same polyline on Google Map as in step 1. On console window, you will see the following log message:

```
info: GPS queued for background processing
info: Received data {"type":"Buffer","data":[49,44,51,57,46,53,51,48,56,57,53,44,45,49,49,57,46,56,49,52,57,55,50]}
info: Received data string 1,39.530895,-119.814972
info: Received request to process (39.530895, -119.814972)
```

#### Step 3 - Deploy and test the app at Google Cloud compute engine

We now are ready to deploy and test the utilityApp at Google Cloud compute engine. 

    * Create an compute engine instance following [this instruction](https://cloud.google.com/compute/docs/quickstart-linux).
    
    * Create new cloud repository following [this instruction](https://cloud.google.com/source-repositories/docs/quickstart)

    * Push local webapp to Google cloud repository we just created.

    * Pull the webapp code from cloud repository to the Google compute engine.

    * Set TEST = false in books/crud.js

    * On cloud compute engine ssh terminal, type `npm start`

```
user@mlplay:~/proj/helium/amr-gps/webapp/assert_track$ npm start
> nodejs-getting-started@1.0.0 start /home/user/proj/helium/amr-gps/webapp/assert_track
> node ${SCRIPT:-app.js}
App listening on port 8080
```

On your browser, type "http://<gcp-external-ip>:8080/books/gps"

Make sure your Helium Atom and Element are on. On your raspberry pi, type `python gps_emulator.py` to start GPS data streaming emulation. You will start to see the received message logging on ssh console window and the GPS tracking on Google Map of your browser.

```
info: Received data {"type":"Buffer","data":[49,44,48,48,48,48,48,48,48,48,54,98,97,56,50,102,102,52,44,51,57,46,52
,56,52,49,50,49,54,54,56,44,45,49,49,57,46,56,53,52,50,52,51,52,53,52]}
info: Received data string 1,000000006ba82ff4,39.484121668,-119.854243454
info: Device cpuserial 000000006ba82ff4
info: Received request to process (39.484121668, -119.854243454)
```

Please note that the "Device cpuserial 000000006ba82ff4" above can be used to track multiple vehicles in the future. The stream data flow to Helium dashboard and Google API dashboard are shown below:

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/bde0c1deb2d0db2b1fb3152c4fd23b14f46b41c9/raspberrypi/images/Helium_Dashboard.png)

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/bde0c1deb2d0db2b1fb3152c4fd23b14f46b41c9/raspberrypi/images/Google_IoT_dashboard.png)

    The path of test vehicle is lively plotted on Google Map.

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/bde0c1deb2d0db2b1fb3152c4fd23b14f46b41c9/raspberrypi/images/utility_truck_tracking.png)

You can also download [this video](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/images/utility_truck_tracking.mov) to watch the vehicle travel path.


## Cloud Pub/Sub to store AMR meter data to Cloud SQL
  
The streamed AMR meter data can be stored in MySQL installed in Google compute engine instance or [Cloud SQL for MySQL](https://cloud.google.com/sql/docs/mysql/quickstart) after installing [cloud_sql_proxy](https://cloud.google.com/sql/docs/mysql/connect-admin-proxy). In this case, a local installed MySQL on Google compute engine is used. But the code [*gce/amr_data_dump.py*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/gce/amr_data_dump.py) works for both cases. The program receives messages from cloud pub/sub subscription then insert data to MySQL database. The procedure is as following:

* Fire off the above script

```
user@mlplay:~/proj/helium/amr-gps/webapp/assert_track/gce$ python amr_data_dump.py
Listening for messages on projects/<projectID>/subscriptions/test
```

* Make sure Helium Atom and Element are both on. Then launch AMR meter reading emulator.

```
pi@raspberrypi:~/proj/amr_fleet_mon $ python amr_emulator.py
```

* On Google GCE ssh window, you will see the following log message

```
Received message: Message {
  data: '2,000000006ba82ff4,70005053,30,249723,scm+'
  attributes: {
    "deviceId": "Helium-6081f9fffe0020b5",
    "deviceNumId": "2651503470551370",
    "deviceRegistryId": "helium_1ff5_824a",
    "deviceRegistryLocation": "us-central1",
    "projectId": "prime-byway-201104",
    "subFolder": ""
  }
}
Received message: Message {
  data: '2,000000006ba82ff4,69204056,30,228535,scm+'
  attributes: {
    "deviceId": "Helium-6081f9fffe0020b5",
    "deviceNumId": "2651503470551370",
    "deviceRegistryId": "helium_1ff5_824a",
    "deviceRegistryLocation": "us-central1",
    "projectId": "prime-byway-201104",
    "subFolder": ""
  }
}
```

* Log into MySQL database to see the stored data

```
MariaDB [amr]> select * from meters;
+---------------------+------------------+----------+----------+-------------+---------+---------+
| time                | cpuserial        | MeterID  | ERT_Type | Consumption | MsgType | entryID |
+---------------------+------------------+----------+----------+-------------+---------+---------+
| 2018-05-30 05:25:50 | 000000006ba82ff4 | 70005053 |       30 |      249723 | scm+    |       1 |
| 2018-05-30 05:25:58 | 000000006ba82ff4 | 69204056 |       30 |      228535 | scm+    |       2 |
| 2018-05-30 05:26:07 | 000000006ba82ff4 | 19066998 |       13 |      146857 | scm     |       3 |
| 2018-05-30 05:26:57 | 000000006ba82ff4 | 70754698 |       30 |      213690 | scm+    |       4 |
| 2018-05-30 05:27:28 | 000000006ba82ff4 | 68572589 |       30 |      189083 | scm+    |       5 |
| 2018-05-30 05:27:40 | 000000006ba82ff4 | 72709793 |       30 |      405181 | scm+    |       6 |
| 2018-05-30 05:28:03 | 000000006ba82ff4 | 70238270 |       30 |      259700 | scm+    |       7 |
| 2018-05-30 05:28:56 | 000000006ba82ff4 | 70756051 |       30 |      239290 | scm+    |       8 |
| 2018-05-30 05:31:02 | 000000006ba82ff4 | 71574730 |       30 |      229490 | scm+    |       9 |
| 2018-05-30 05:31:14 | 000000006ba82ff4 | 73023040 |       30 |      182555 | scm+    |      10 |
| 2018-05-30 05:31:23 | 000000006ba82ff4 | 33048427 |       11 |      228948 | scm     |      11 |
| 2018-05-30 05:31:37 | 000000006ba82ff4 | 71727896 |       30 |      135746 | scm+    |      12 |
| 2018-05-30 05:31:42 | 000000006ba82ff4 | 70009823 |       30 |      248076 | scm+    |      13 |
+---------------------+------------------+----------+----------+-------------+---------+---------+
13 rows in set (0.00 sec)
```

## Future development

I would pitch this project to utility companies and hope to generate some interests. The future work may include:

* Design a case for hardware components that can be securely mounted on utility vehicles
* Polish the Node.js web and Google Map application to include multi-vehicle tracking
* Test Helium cellular access point on board a utility vehicle
