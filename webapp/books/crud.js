// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

const TEST = false;
const express = require('express');
const Pubsub = require('@google-cloud/pubsub');
const images = require('../lib/images');
const oauth2 = require('../lib/oauth2');
const background = require('../lib/background');
const fs = require('fs');
const config = require('../config');
const logging = require('../lib/logging');

const topicName = config.get('TOPIC_NAME');
const subscriptionName = config.get('SUBSCRIPTION_NAME');

const pubsub = Pubsub({
  projectId: config.get('GCLOUD_PROJECT')
});

const GPS_DATATYPE = 1;
const METER_DATATYPE = 2;

function getModel () {
  return require(`./model-${require('../config').get('DATA_BACKEND')}`);
}

const router = express.Router();

// Use the oauth middleware to automatically get the user's profile
// information and expose login/logout URLs to templates.
router.use(oauth2.template);

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
  res.set('Content-Type', 'text/html');
  next();
});


function subscribe (req, res) {
  if (req.headers.accept && req.headers.accept == 'text/event-stream') {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
  // Subscribe to Cloud Pub/Sub and receive messages to process books.
  // The subscription will continue to listen for messages until the process
  // is killed.
  // [START subscribe]
  const unsubscribeFn = background.subscribe((err, data) => {
    // Any errors received are considered fatal.
    if (err) {
      throw err;
    }
    if (req.url == '/gps') {
      var str_array;
      var datatype;
      var lat, lng;
      logging.info('Received data ' + JSON.stringify(data));
      if ("datatype" in data) {
        datatype = data.datatype
        if (datatype == GPS_DATATYPE) {
          lat = data.lat;
          lng = data.lng;
        }
      } else {
          var dataObj = JSON.parse(JSON.stringify(data));
          if (dataObj.type == "Buffer") {
            var mystr = String.fromCharCode.apply(null, dataObj.data);
            logging.info(`Received data string ${mystr}`);
            str_array = mystr.split(',');
            datatype = parseInt(str_array[0]);
            if (datatype == GPS_DATATYPE) {
              if (TEST) {
                lat = parseFloat(str_array[1]);
                lng = parseFloat(str_array[2]);
              } else {
                // id will be used in the future for tracking 
                // multiple utility trucks
                var id = str_array[1]
                logging.info('Device cpuserial ' + id);
                lat = parseFloat(str_array[2]);
                lng = parseFloat(str_array[3]);
              }
            }
          }
      }

      if (datatype == GPS_DATATYPE) {
        logging.info(`Received request to process (${lat}, ${lng})`);
        //processBook(data.bookId);
        var id = (new Date()).toLocaleTimeString();
        res.write('id: ' + id + '\n');
        res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
      } else if (datatype == METER_DATATYPE) {
        logging.info(`Received request to process ${data}`);
      } else {
        logging.warn('Unknown data type');
      }
    } else {
      logging.warn('Unknown request');
    }
  });
  // [END subscribe]
  return unsubscribeFn;
  } else {
    res.writeHead(200, {
        'Content-Type': 'text/html',
    });
    res.write(fs.readFileSync('./views/books/assert_track.html'));
    res.end();
  }
}

function test(req, res) {
    if (req.headers.accept && req.headers.accept == 'text/event-stream') {
      res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
      });
        var lat = 39.530895;
        var lng = -119.814972;
        setInterval(function() {
            var id = (new Date()).toLocaleTimeString();
            res.write('id: ' + id + '\n');
            lat = lat + 0.001;
            lng = lng + 0.001;
            res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
	    }, 500);
        res.write("data: " + JSON.stringify({"lat": lat, "lng": lng}) + '\n\n');
    } else {
        res.writeHead(200, {
            'Content-Type': 'text/html',
        });
        res.write(fs.readFileSync('./views/books/assert_track.html'));
        res.end();
    }
}

function test_publish() {
  const publishFn = background.getTopic((err, topic) => {
    if (err) {
      logging.error('Error occurred while getting pubsub topic', err);
      return;
    }

    const publisher = topic.publisher();
    var lat = 39.530895;
    var lng = -119.814972;
    var data = {
      datatype: GPS_DATATYPE,
      lat: lat,
      lng: lng
    }
    var mystr = data.datatype.toString() + ',' + data.lat.toString() + ',' + data.lng.toString();
    setInterval(function() {
      lat = lat + 0.001;
      lng = lng + 0.001;
      data = {
        datatype: GPS_DATATYPE,
        lat: lat,
        lng: lng
      }
      mystr = data.datatype.toString() + ',' + data.lat.toString() + ',' + data.lng.toString();
      //publisher.publish(Buffer.from(JSON.stringify(data)), (err) => {
      publisher.publish(Buffer.from(mystr), (err) => {
        if (err) {
          logging.error('Error occurred while publishing GPS data', err);
        } else {
          logging.info('GPS queued for background processing');
        }
      });
    }, 500);
    //publisher.publish(Buffer.from(JSON.stringify(data)), (err) => {
    publisher.publish(Buffer.from(mystr), (err) => {
      if (err) {
        logging.error('Error occurred while publishing GPS data', err);
      } else {
        logging.info(`GPS queued for background processing`);
      }
    });
  });
}

/**
 * GET /books
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/', (req, res, next) => {
  getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
    if (err) {
      next(err);
      return;
    }
    res.render('books/location.html');
    if (TEST) {
      test_publish();
    }
    /*res.render('books/list.pug', {
      books: entities,
      nextPageToken: cursor
    });*/
  });
});

/**
 * GET /gps
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/gps', (req, res, next) => {
  getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
    if (err) {
      next(err);
      return;
    }
    //test(req, res);
    subscribe(req, res);
  });
});

// Use the oauth2.required middleware to ensure that only logged-in users
// can access this handler.
router.get('/mine', oauth2.required, (req, res, next) => {
  getModel().listBy(
    req.user.id,
    10,
    req.query.pageToken,
    (err, entities, cursor, apiResponse) => {
      if (err) {
        next(err);
        return;
      }
      res.render('books/list.pug', {
        books: entities,
        nextPageToken: cursor
      });
    }
  );
});

/**
 * GET /books/add
 *
 * Display a form for creating a book.
 */
router.get('/add', (req, res) => {
  res.render('books/form.pug', {
    book: {},
    action: 'Add'
  });
});

/**
 * POST /books/add
 *
 * Create a book.
 */
// [START add]
router.post(
  '/add',
  images.multer.single('image'),
  images.sendUploadToGCS,
  (req, res, next) => {
    const data = req.body;

    // If the user is logged in, set them as the creator of the book.
    if (req.user) {
      data.createdBy = req.user.displayName;
      data.createdById = req.user.id;
    } else {
      data.createdBy = 'Anonymous';
    }

    // Was an image uploaded? If so, we'll use its public URL
    // in cloud storage.
    if (req.file && req.file.cloudStoragePublicUrl) {
      data.imageUrl = req.file.cloudStoragePublicUrl;
    }

    // Save the data to the database.
    getModel().create(data, true, (err, savedData) => {
      if (err) {
        next(err);
        return;
      }
      res.redirect(`${req.baseUrl}/${savedData.id}`);
    });
  }
);
// [END add]

/**
 * GET /books/:id/edit
 *
 * Display a book for editing.
 */
router.get('/:book/edit', (req, res, next) => {
  getModel().read(req.params.book, (err, entity) => {
    if (err) {
      next(err);
      return;
    }
    res.render('books/form.pug', {
      book: entity,
      action: 'Edit'
    });
  });
});

/**
 * POST /books/:id/edit
 *
 * Update a book.
 */
router.post(
  '/:book/edit',
  images.multer.single('image'),
  images.sendUploadToGCS,
  (req, res, next) => {
    const data = req.body;

    // Was an image uploaded? If so, we'll use its public URL
    // in cloud storage.
    if (req.file && req.file.cloudStoragePublicUrl) {
      req.body.imageUrl = req.file.cloudStoragePublicUrl;
    }

    getModel().update(req.params.book, data, true, (err, savedData) => {
      if (err) {
        next(err);
        return;
      }
      res.redirect(`${req.baseUrl}/${savedData.id}`);
    });
  }
);

/**
 * GET /books/:id
 *
 * Display a book.
 */
router.get('/:book', (req, res, next) => {
  getModel().read(req.params.book, (err, entity) => {
    if (err) {
      next(err);
      return;
    }
    res.render('books/view.pug', {
      book: entity
    });
  });
});

/**
 * GET /books/:id/delete
 *
 * Delete a book.
 */
router.get('/:book/delete', (req, res, next) => {
  getModel().delete(req.params.book, (err) => {
    if (err) {
      next(err);
      return;
    }
    res.redirect(req.baseUrl);
  });
});

/**
 * Errors on "/books/*" routes.
 */
router.use((err, req, res, next) => {
  // Format error and forward to generic error handler for logging and
  // responding to the request
  err.response = err.message;
  next(err);
});

module.exports = router;
