'use strict';

// Imports the Google Cloud client library
const PubSub = require('@google-cloud/pubsub');

// Your Google Cloud Platform project ID
const projectId = 'prime-byway-201104';

// Instantiates a client
const pubsub = new PubSub({
    projectId: projectId,
});

// The name for the new topic
const topicName = 'amr-gps';

function publishMessage(topicName, data) {
    // [START pubsub_publish_message]
	// Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
    const dataBuffer = Buffer.from(data);

	pubsub
        .topic(topicName)
        .publisher()
        .publish(dataBuffer)
        .then(messageId => {
            console.log(`Message ${messageId} published.`);
        })
        .catch(err => {
            console.error('ERROR:', err);
        });
    // [END pubsub_publish_message]
}

function listenForMessages(subscriptionName, timeout) {
    // [START pubsub_listen_messages]

  	// References an existing subscription
    const subscription = pubsub.subscription(subscriptionName);

    // Create an event handler to handle messages
    let messageCount = 0;
    const messageHandler = message => {
        console.log(`Received message ${message.id}:`);
        console.log(`\tData: ${message.data}`);
        console.log(`\tAttributes: ${message.attributes}`);
    	messageCount += 1;

    	// "Ack" (acknowledge receipt of) the message
        message.ack();

        // Redraw path polyline
        redraw(message.data);
    };

    // Listen for new messages until timeout is hit
    subscription.on(`message`, messageHandler);
    setTimeout(() => {
        subscription.removeListener('message', messageHandler);
        console.log(`${messageCount} message(s) received.`);
    }, timeout * 1000);
    // [END pubsub_listen_messages]
}

var listener = listenForMessages('test', 60);

window.lat = 39.530895;
window.lng = -119.814972;

window.initMap = initMap;

// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
var map, infoWindow;
var mark;
var lineCoords = [];
var initMap = function() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: 8
    });
    mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('You are here');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
 };

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function redraw(coordinates) {
    lat = coordinates.lat;
    lng = coordinates.lng;

    //map.setCenter({lat:lat, lng:lng, alt:0});
    mark.setPosition({lat:lat, lng:lng, alt:0});
      
    lineCoords.push(new google.maps.LatLng(lat, lng));

    var lineCoordinatesPath = new google.maps.Polyline({
        path: lineCoords,
        geodesic: true,
        strokeColor: '#2E10FF'
    });
      
    lineCoordinatesPath.setMap(map);
}

setInterval(function() {
    publishMessage(topicName, {lat:window.lat + 0.001, lng:window.lng + 0.01});}, 500);
