/#!/usr/bin/python

import os, sys
# Imports the Google Cloud client library
from google.cloud import pubsub_v1

import MySQLdb
import time


# Cloud Project Variables
GCLOUD_PROJECT = 'prime-byway-201104'
TOPIC_NAME = 'amr-gps'
SUBSCRIPTION_NAME = 'test'
CLOUDSQL_CONNECTION_NAME = 'prime-byway-201104:us-west1:ming-play-mysql-db1'
CLOUDSQL_USER = 'mliu'
CLOUDSQL_PASSWORD = 'MaoY0uhw'
CLOUDSQL_DATABASE = 'amr'

GPS_DATATYPE = 1
METER_DATATYPE = 2
db = None

def connect_to_cloudsql():
    global db

    # Try to connect using TCP. This
    # will work if you're running a local MySQL server or using the Cloud SQL
    # proxy, for example:
    #
    #   $ cloud_sql_proxy -instances=your-connection-name=tcp:3306
    #
    try:
        db = MySQLdb.connect(
            host='127.0.0.1', user=CLOUDSQL_USER, passwd=CLOUDSQL_PASSWORD,
                db=CLOUDSQL_DATABASE, charset='utf8')
    except MySQLdb.Error as e:
        print(e)
        sys.exit(2)
        

def insert_data(data):
    global db

    mylist = data.split(',')
    if int(mylist[0]) == METER_DATATYPE:
        sql_str = """INSERT OR REPLACE INTO meters (cpuserial, 
            MeterID, ERT_TYPE, Consumption, MsgType)
            VALUES('{0}', '{1}', '{2}', '{3}', '{4}')"""
        with db:
            db.cursor().execute(mylist[1], mylist[2], mylist[3], mylist[4], mylist[5]) 


def receive_messages(project, subscription_name):
    """Receives messages from a pull subscription."""
    # [START pubsub_subscriber_async_pull]
    # [START pubsub_quickstart_subscriber]
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message))
        message.ack()
        insert_data(messae.data)


    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)
    # [END pubsub_subscriber_async_pull]
    # [END pubsub_quickstart_subscriber]


def main():
    connect_to_cloudsql()
    receive_messages(GCLOUD_PROJECT, SUBSCRIPTION_NAME)

if __name__ == "__main__":
    main()


