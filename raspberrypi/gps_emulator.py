#!/usr/bin/env python

import os, sys
import time
import string
from datetime import datetime, timedelta
from helium_client import Helium
from dateutil import parser

import dbApp

# Check gpsd is running. If not launch it.
GPS_DATATYPE = 1
METER_DATATYPE = 2

try:
    helium = Helium("/dev/serial0")
    helium.connect()
    channel = helium.create_channel("Google IoT Core")
except:
    print "Cannot open helium channel, exiting..."
    sys.exit(2)


db_dir = '/var/lib/helium'

db_name = db_dir + '/gps_meters.db'
mydbApp = dbApp.dbApp(db_name)

sql_str = """SELECT time, cpuserial, lat, lon from gps
             WHERE datetime(time) > datetime('2018-05-20T00:00:00Z')"""
mydbApp.exe_sql(sql_str)

rows = mydbApp.fetchall()

count = 0
pre_timestamp = parser.parse('2018-05-20T00:00:00Z')
for row in rows:
    timestamp = parser.parse(row[0])
    time_diff = (timestamp - pre_timestamp).total_seconds()
    if time_diff >= 1:
        mylist = [GPS_DATATYPE]
        mylist.extend(row[1:])
        myList = ','.join(map(str, mylist))
        print myList
        if count > 0:
            print "Wait time={} s".format(time_diff)
            time.sleep(time_diff)
        try:
            channel.send(myList)
        except: 
            print "Connection dropped, attempt to reconnect..."
            channel = None
            time.sleep(1)
            if helium.connected() == False:
                helium.close()
                try:
                    helium = Helium("/dev/serial0")
                    helium.connect()
                    channel = helium.create_channel("Google IoT Core")
                except:
                    print "Cannot open helium channel, exiting..."
                    sys.exit(2)
            else:
                try:
                    channel = helium.create_channel("Google IoT Core")
                except:
                    print "Cannot open helium channel, exiting..."
                    sys.exit(2)
            channel.send(myList)

        pre_timestamp = timestamp
        count = count + 1

mydbApp.db_close()
