#!/usr/bin/env python

import subprocess
import Queue
import threading
import logging
import sys, os
import time
import signal
import json
from datetime import datetime, timedelta
from helium_client import Helium
from traceback import print_exc
from calendar import timegm
from dateutil import parser
from utility import *

class rtlamrThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(),
            kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name,
            verbose=verbose)
        self.client_sock = args[0]
        self.device_index = args[1]
        self.queue = args[2]
        self.user = args[3]
        self.rtltcp_opts = args[4]
        self.rtlamr_opts = args[5]
        self.msgtype = 'scm'
        if "msgtype" in self.rtlamr_opts:
            self.msgtype = self.rtlamr_opts["msgtype"]

        self.out_q = Queue.Queue()
        self.result ={}
        self.cpuserial = get_serial()    # Get Raspberry Pi CPU Serial
        self.rtlamr_status = 'stopped'

    def run(self):
        GPS_DATATYPE = 1
        METER_DATATYPE = 2
        command = ['/home/pi/gocode/bin/rtlamr']
        if self.rtltcp_opts:
            for k, v in self.rtltcp_opts.items():
                command.append('-' + k + '=' + v)
        if self.rtlamr_opts:
            for k, v in self.rtlamr_opts.items():
                command.append('-' + k + '=' + v)
        #print "command={}".format(command)

        try:
            rtlamr_pid = subprocess.Popen(command, stdout=subprocess.PIPE)
            self.rtlamr_status = 'running'
            logging.debug(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') + ": running rtlamr with {}".format(self.rtlamr_opts))
        except subprocess.CalledProcessError as error:
            err_msg = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + str(error.output)
            logging.critical(err_msg)
            #sys.exit(2)
            return 

        t = threading.Thread(target=self.enqueue_output, 
                args=(rtlamr_pid.stdout, self.out_q))
        t.setDaemon(True)
        t.start()

        db_dir = '/var/lib/helium'
        db_name = db_dir + '/gps_meters.db'
        mydbApp = dbApp.dbApp(db_name)

        while True:
            try: line = self.out_q.get_nowait()
            except Queue.Empty:
                time.sleep(0.01)
            else:
                if "Time" in line and "Consumption" in line:
                    mystr = line.rstrip('\n')
                        
                    try:
                        data = json.loads(mystr)
                    except ValueError, e:
                        logging.warning("JSON Error: {} {}".format(mystr, str(e)))
                        continue

                    self.result['DateTime'] = data["Time"]
                    if self.msgtype == 'scm+':
                        self.result['MeterID'] =  data["Message"]["EndpointID"]
                        self.result['ERT_Type'] = data["Message"]["ProtocolID"]
                        self.result['TamperPhy'] = data["Message"]["Tamper"]
                        self.result['TamperEnc'] = data["Message"]["EndpointType"]
                        self.result['Consumption'] = data["Message"]["Consumption"]
                    else:
                        self.result['MeterID'] =  data["Message"]["ID"]
                        self.result['ERT_Type'] = data["Message"]["Type"]
                        self.result['TamperPhy'] = data["Message"]["TamperPhy"]
                        self.result['TamperEnc'] = data["Message"]["TamperEnc"]
                        self.result['Consumption'] = data["Message"]["Consumption"]

                    offset = self.result['DateTime'][-6:]
                    offset_list = offset.split(':')
                    UTC_OFFSET = int(offset_list[0]) + int(offset_list[1])/60
                    localtime = parser.parse(self.result['DateTime'][:-6])
                    utctime = localtime - timedelta(hours=UTC_OFFSET)
                    sql_str = """INSERT OR REPLACE INTO amr (time, 
                                      cpuserial, 
                                      MeterID, 
                                      ERT_Type, 
                                      TamperPhy, 
                                      TamperEnc, 
                                      Consumption, 
                                      MsgType)
                               VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')"""

                    mydbApp.exe_sql(sql_str.format(utctime.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                                       self.cpuserial, 
                                       self.result['MeterID'],
                                       self.result['ERT_Type'], 
                                       self.result['TamperPhy'], 
                                       self.result['TamperEnc'], 
                                       self.result['Consumption'], 
                                       self.msgtype))

                    if self.client_sock != None:
                        #epoch_time = timegm(utctime.timetuple())
                        #mylist = [METER_DATATYPE, epoch_time, self.cpuserial, self.result['MeterID'], self.result['ERT_Type'], 
                        #          self.result['TamperPhy'], self.result['TamperEnc'], self.result['Consumption'], self.msgtype]
                        mylist = [METER_DATATYPE, self.cpuserial, self.result['MeterID'], self.result['ERT_TYPE'], self.result['Consumption'], self.msgtype] 
                        myList = ','.join(map(str, mylist))
                        logging.debug("Meter Reading: " + myList)
                        try:
                            self.client_sock.send(myList)
                        except Exception as e:
                            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
                            logging.error(timestamp + ': Exception ' + e.__class__.__name__ + 'happened')
                            print_exc()

                elif "Time Limit Reached" in line:
                    self.out_q.task_done()
                    self.queue.task_done()
                    mydbApp.db_close()
                    break;

            try:
                msg = self.queue.get_nowait()
                if msg == "Task finished":
                    self.queue.task_done()
                    mydbApp.db_close()
                    break
            except Queue.Empty:
                continue


    def enqueue_output(self, out, queue):
        for line in iter(out.readline, ''):
            print line
            queue.put(line)
        out.close()

    def stop(self):
        self.rtlamr_status = 'stopped'
