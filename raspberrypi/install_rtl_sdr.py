#!/usr/bin/env python

import sys, os
import subprocess

def install_gnuradio():
    output = subprocess.check_output(['lsb_release', '-c'])
    if "wheezy" in output:
        in_fname = "/etc/apt/sources.list"
        out_fname = "sources.list"
        try:
            in_f = open(in_fname, 'r')
        except IOError:
            print "Error: can't oepn file %s for reading" % in_fname
            sys.exit(2)

        try:
            out_f = open(out_fname, 'wb')
        except IOError:
            print "Error: can't oepn file %s for writing" % out_fname
            sys.exit(2)

        for line in in_f:
            out_f.write(line)
        out_f.write("deb http://archive.raspbian.org/raspbian jessie main\n")

        in_f.close()
        out_f.close()
        cmd = ['sudo', 'chown', 'root:root', out_fname]
        subprocess.check_call(cmd)
        cmd = ['sudo', 'mv', '-b', out_fname, in_fname]
        subprocess.check_call(cmd)

        print "updating Apt cache, please wait..."
        cmd = ['sudo', 'apt-get', 'update']
        subprocess.check_call(cmd)

    print "install gnu_radio, please wait..."
    cmd = ['sudo', 'apt-get', 'install', 'gnuradio', 'gnuradio-dev']
    subprocess.call(cmd)

def setup_rtl_sdr():
    in_fname = "/etc/modprobe.d/raspi-blacklist.conf"
    out_fname = "raspi-blacklist.conf"

    try:
        out_f = open(out_fname, 'wb')
    except IOError:
        print "Error: can't oepn file %s for writing" % out_fname
        sys.exit(2)

    mystring = "blacklist dvb_usb_rtl28xxu\n"
    str_found = 0

    if os.path.isfile(in_fname):
        in_f = open(in_fname, 'r')
        for line in in_f:
            out_f.write(line)
            if mystring in line:
                str_found = 1
        in_f.close()

    if str_found == 0:
        out_f.write(mystring)

    out_f.close()

    cmd = ['sudo', 'chown', 'root:root', out_fname]
    subprocess.call(cmd)
    cmd = ['sudo', 'mv', '-b', out_fname, in_fname]
    subprocess.call(cmd)

    print "install rtl-sdr software and GNU Radio support, pelase wait..."
    cmd = ['sudo', 'apt-get', 'install', 'rtl-sdr', 'gr-osmosdr']
    subprocess.call(cmd)

    out_str = subprocess.check_output('lsusb')
    try:
        index = out_str.index('Realtek Semiconductor Corp.')
    except ValueError:
        print "Error: No DVB-T USB dongle present, exiting..."
        sys.exit(2)

    index = out_str.rfind("ID", 0, index)
    vendor = out_str[(index+3):(index+7)]    #0bda
    product = out_str[(index+8):(index+13)]  #2838 or 2832

    in_fname = "/etc/udev/rules.d/20.rtlsdr.rules"
    out_fname = "20.rtlsdr.rules"

    try:
        out_f = open(out_fname, 'wb')
    except IOError:
        print "Error: can't oepn file %s for writing" % out_fname
        sys.exit(2)

    mystring = 'SUBSYSTEM=="usb", ATTRS{idVendor}=="' + vendor + \
               '", ATTRS{idProduct}=="' + product + \
               '", GROUP="adm", MODE="0666", SYMLINK+="rtl_sdr"\n'
    str_found = 0

    if os.path.isfile(in_fname):
        in_f = open(in_fname, 'r')
        for line in in_f:
            out_f.write(line)
            if mystring in line:
                str_found = 1
        in_f.close()

    if str_found == 0:
        out_f.write(mystring)

    out_f.close()

    cmd = ['sudo', 'chown', 'root:root', out_fname]
    subprocess.check_call(cmd)
    cmd = ['sudo', 'mv', '-b', out_fname, in_fname]
    subprocess.check_call(cmd)


def main():
    if subprocess.call(['which', 'rtl_sdr']):
        install_gnuradio()
        setup_rtl_sdr()


if __name__ == "__main__":
    main()
