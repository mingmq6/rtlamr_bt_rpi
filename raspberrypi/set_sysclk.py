#!/usr/bin/env python

import os, sys
import subprocess
import gps
from utility import start_gpsd

# Check gpsd is running. If not launch it.
start_gpsd()

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

while True:
    try:
        report = session.next()
        # Wait for a 'TPV' report and display the current time
        # To see all report data, uncomment the line below
        # print report
        if report['class'] == 'TPV':
            if hasattr(report, 'time'):
                print "current time: " + report.time
                command = ['sudo', 'date', '-s', report.time]
                subprocess.call(command)
                session = None
                quit()
    except KeyError:
        pass
    except KeyboardInterrupt:
        quit()
    except StopIteration:
        session = None
        print "GPSD has terminated"
