#!/usr/bin/env python

import os, sys
import logging
import glob
import time
import json
import string
import getopt, re
import signal
import gps
from datetime import datetime
from helium_client import Helium
from calendar import timegm
from traceback import print_exc

from utility import *
import dbApp
import AMR

# Check gpsd is running. If not launch it.
start_gpsd()
GPS_DATATYPE = 1
METER_DATATYPE = 2
# Only send gps coordinate for every gps_interval read
gps_interval = 10

directory = '/var/log'
if os.path.isdir(directory) == False:
    os.makedirs(directory)

mylog = directory + '/utilityApp.log'
logging.basicConfig(filename=mylog, level=logging.DEBUG)

try:
    helium = Helium("/dev/serial0")
    helium.connect()
    channel = helium.create_channel("Google IoT Core")
except:
    channel = None

apps = []

db_dir = '/var/lib/helium'
if os.path.isdir(db_dir) == False:
    os.makedirs(db_dir)

db_name = db_dir + '/gps_meters.db'
mydbApp = dbApp.dbApp(db_name)
sql_str = """CREATE TABLE IF NOT EXISTS amr (
                time DATETIME PRIMARY KEY,
                cpuserial TEXT NOT NULL,
                MeterID INTEGER NOT NULL,
                ERT_Type TINYINT,
                TamperPhy SMALLINT,
                TamperEnc SMALLINT,
                Consumption INTEGER,
                MsgType VARCHAR(8)) """

mydbApp.exe_sql(sql_str)

sql_str = """CREATE TABLE IF NOT EXISTS gps (
                time DATETIME PRIMARY KEY,
                cpuserial TEXT NOT NULL,
                lon FLOAT NOT NULL,
                lat FLOAT NOT NULL,
                alt FLOAT NOT NULL,
                speed FLOAT NOT NULL,
                climb FLOAT NOT NULL,
                track FLOAT NOT NULL,
                epx FLOAT,
                epy FLOAT,
                epv FLOAT,
                ept FLOAT,
                mode TINYINT NOT NULL,
                device TEXT,
                class TEXT) """

mydbApp.exe_sql(sql_str)

#if channel != None:
#    mydbApp.db_close()

def usage():
    msg = """
Usage: python utilityApp.py [--opts_file]
       Utility truck fleet monitor and automatic utility meter reading with SDR
       --help (h)         : returns this message
       --opts_file        : optional rtl_tcp and rtlamr run options in json format
           The rtlamr and rtltcp usages are:
           Usage of rtlamr:
             -duration=0: time to run for, 0 for infinite, ex. 1h5m10s
             -filterid=: display only messages matching an id in a comma-separated list of ids.
             -filtertype=: display only messages matching a type in a comma-separated list of types.
             -format=plain: format to write log messages in: plain, csv, json, or xml
             -msgtype=scm: message type to receive: scm, scm+, idm, r900 and r900bcd
             -samplefile=/dev/null: raw signal dump file
             -single=false: one shot execution, if used with -filterid, will wait for exactly one packet from each meter id
             -symbollength=72: symbol length in samples
             -unique=false: suppress duplicate messages from each meter
             -version=false: display build date and commit hash

           rtltcp specific:
             -agcmode=false: enable/disable rtl agc
             -centerfreq=0: center frequency to receive on
             -directsampling=false: enable/disable direct sampling
             -freqcorrection=0: frequency correction in ppm
             -gainbyindex=0: set gain by index
             -offsettuning=false: enable/disable offset tuning
             -rtlxtalfreq=0: set rtl xtal frequency
             -samplerate=0: sample rate
             -server=127.0.0.1:1234: address or hostname of rtl_tcp instance
             -testmode=false: enable/disable test mode
             -tunergain=0: set tuner gain in dB
             -tunergainmode=false: enable/disable tuner gain
             -tunerxtalfreq=0: set tuner xtal frequency

           The above usage is translated to follwing sample json file format:
           [
             {
                "duration": "1h5m10s",
                "filterid": ["123456", "456789"],
                "filtertype": ["11", "13"],
                "format": "json",
                "msgtype": "scm",
                "samplefile": "/dev/null",
                "single": "false",
                "server": "127.0.0.1:1234"
             },
             {
                "duration": "1h5m10s",
                "filterid": ["654321", "987654"],
                "filtertype": ["11", "13"],
                "format": "json",
                "msgtype": "scm",
                "samplefile": "/dev/null",
                "single": "false",
                "server": "127.0.0.1:1235"
             }
           ]
"""
    print msg

def app_start():
    global apps

    timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    logging.info(timestamp + ": starting meter read...")
    for device_index in range(len(apps)):
        rtltcp_opts = apps[device_index]['rtltcp_opts']
        rtlamr_opts = apps[device_index]['rtlamr_opts']
        apps[device_index]['obj'] = AMR.AMR(client_sock=channel, rtltcp_opts=rtltcp_opts, rtlamr_opts=rtlamr_opts, device_index=device_index)
        apps[device_index]['obj'].start()
        apps[device_index]['rtlamr_status'] = 'running'
        apps[device_index]['rtltcp_status'] = 'running'

def app_terminate():
    global apps
    for app in apps: 
        if app['obj'] != None:
            app['obj'].terminate()
            app['rtlamr_status'] = 'stopped'
            app['rtltcp_status'] = 'stopped'
            app['obj'] = None


def main():
    global apps
    global channel
    global mydbApp

    opts_file = "/home/pi/proj/amr_fleet_mon/rtlamr_opts.config"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", \
                ["help", "opts_file="])

        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
                sys.exit(0)
            elif opt == '--opts_file':
                opts_file = arg
            else:
                assert False, "Unhandled option"
                usage()
                sys.exit(2)
    except getopt.GetoptError as err:
        usage()
        sys.exit(2)

    opts = []
    if os.path.isfile(opts_file):
        opts = json.load(open(opts_file))

    device_count = get_sdr_device_count()
    logging.info("device_count={}".format(device_count))
    if device_count == 0:
        logging.error("No SDR dongle present, exiting...")
        sys.exit(2)
    
    msgtypes = ['scm', 'scm+', 'idm', 'r900', 'r900bcd']
    for device_index in range(device_count):
        app = {}
        app['obj'] = None
        app['rtlamr_status'] = 'stopped'
        app['rtltcp_status'] = 'stopped'
        if opts and device_index < len(opts):
            app['rtltcp_opts'] = opts[device_index]['rtltcp_opts'] 
            app['rtlamr_opts'] = opts[device_index]['rtlamr_opts'] 

            if not 'server' in app['rtltcp_opts']:
                app['rtltcp_opts'] = {'server': '127.0.0.1:' + str(1234 + device_index)}

            if not 'msgtype' in app['rtlamr_opts']:
                app['rtlamr_opts']['msgtype'] = msgtypes[device_index]
        else:
            app['rtltcp_opts'] = {'server': '127.0.0.1:' + str(1234 + device_index)}
            app['rtlamr_opts'] = {'unique': 'true', 'msgtype': msgtypes[device_index]}
        

        # Force json data type
        app['rtlamr_opts']['format'] = 'json'

        apps.append(app)

    # Listen on port 2947 (gpsd) of localhost
    session = gps.gps("localhost", "2947")
    session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

    time.sleep(10)
    app_start()
    cpuserial = get_serial()

    count = 0
    while True:
        try:
            report = session.next()
            # Wait for a 'TPV' report and display the current time
            # To see all report data, uncomment the line below
            # print report
            if report['class'] == 'TPV':
                mydict = report.__dict__
                gps_str = json.dumps(mydict)
                logging.debug(gps_str)
                #if channel == None:
                sql_str = """INSERT OR REPLACE INTO gps (time, 
                                              cpuserial, 
                                              lon, 
                                              lat, 
                                              alt, 
                                              speed, 
                                              climb, 
                                              track,
                                              epx,
                                              epy,
                                              epv,
                                              ept,
                                              mode,
                                              device,
                                              class)
                            VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', 
                                       '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}')"""
                mydbApp.exe_sql(sql_str.format(mydict['time'], 
                                               cpuserial,
                                               mydict['lon'],
                                               mydict['lat'],
                                               mydict['alt'],
                                               mydict['speed'],
                                               mydict['climb'],
                                               mydict['track'],
                                               mydict['epx'],
                                               mydict['epy'],
                                               mydict['epv'],
                                               mydict['ept'],
                                               mydict['mode'],
                                               mydict['device'],
                                               mydict['class']))
                if channel != None:
                    if count == 0:
                        #utctime = time.strptime(mydict['time'], "%Y-%m-%dT%H:%M:%S.%fZ")
                        #epoch_time = timegm(utctime)
                        #mylist = [GPS_DATATYPE, epoch_time, cpuserial, mydict['lon'], mydict['lat'], mydict['alt'], mydict['speed']]
                        mylist = [GPS_DATATYPE, cpuserial, mydict['lat'], mydict['lon']]
                        myList = ','.join(map(str, mylist))
                        logging.debug("GPS: " + myList)
                        try:
                            channel.send(myList)
                        except Exception as e:
                            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
                            logging.error(timestamp + ': Exception ' + e.__class__.__name__ + 'happened')
                            print_exc()
                    count = count + 1
                    if count == gps_interval:
                        count = 0
        except KeyError:
            pass
        except KeyboardInterrupt:
            app_terminate()
            if channel == None and mydbApp:
                mydbApp.db_close()
            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            logging.info(timestamp + ": all done")
            quit()
        except StopIteration:
            session = None
            if channel == None and mydbApp:
                mydbApp.db_close()
            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            logging.info(timestamp + ": GPSD has terminated")

        job_exists = False
        for app in apps:
            app['rtlamr_status'] =  app['obj'].get_rtlamr_status()
            app['rtltcp_status'] =  app['obj'].get_rtltcp_status()
            if app['obj'].job_done() == False:
                job_exists = True

        if job_exists == False:
            if channel == None and mydbApp:
                mydbApp.db_close()
            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            logging.info(timestamp + ": All done")
            quit()


if __name__ == "__main__":
    main()

