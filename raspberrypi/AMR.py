#!/usr/bin/python

import subprocess
import os, sys
import signal
import Queue
import logging
import json
import time
from datetime import datetime, timedelta
from helium_client import Helium
from utility import *
import threading
import rtltcpThread
import rtlamrThread

class AMR:

    def __init__(self, client_sock, user = None, rtltcp_opts = None, rtlamr_opts = None, device_index = None):
        self.user = user
        self.client_sock = client_sock
        self.rtltcp_opts = rtltcp_opts
        if device_index != None:
            self.device_index = device_index
        else:
            self.device_index = 0

        if "server" in rtltcp_opts:
            serverlist = rtltcp_opts["server"].split(':')
            self.host = serverlist[0]
            self.port = serverlist[1]
        else:
            self.host = '127.0.0.1'
            self.port = '1234'

        self.rtlamr_opts = rtlamr_opts
        if "duration" in rtlamr_opts:
            self.duration = parse_rtlamr_duration(rtlamr_opts["duration"])
        else:
            self.duration = 0
        self.queue = Queue.Queue()
        self.t_rtltcp = None
        self.t_rtlamr = None
        self.jobdone = False

    def launch_rtltcp(self):
        # check rtl_tcp process exists or not, if yes, kill it
        pid = self.get_rtltcp_pid()
        if pid != 0:
            os.kill(pid, signal.SIGKILL)
            logging.debug("Existing pid {} for rtl_tcp is killed".format(pid))

        self.t_rtltcp = rtltcpThread.rtltcpThread(name='rtl_tcp', args=(self.host, self.port, self.device_index))
        self.t_rtltcp.setDaemon(True)
        self.t_rtltcp.start()
        time.sleep(1)
        logging.info('Sleep for 1 sec for rtl_tcp to be ready')
        return

    def launch_rtlamr(self):
        # check rtlamr process exists or not, if yes, kill it
        pid = self.get_rtlamr_pid()
        if pid != 0:
            os.kill(pid, signal.SIGKILL)

        self.t_rtlamr = rtlamrThread.rtlamrThread(name='rtlamr', args=(self.client_sock, 
            self.device_index, self.queue, self.user, self.rtltcp_opts, self.rtlamr_opts))
        self.t_rtlamr.setDaemon(True)
        self.t_rtlamr.start()
                
    def start(self):
        self.launch_rtltcp()
        self.launch_rtlamr()
        time.sleep(1)
        if self.duration != 0:
            self.process_mon()

    def process_mon(self):
        start_time = datetime.now()
        end_time = start_time + timedelta(seconds=self.duration)
        while True:
            if datetime.now() > end_time:
                self.t_rtlamr.out_q.put("Time Limit Reached")
                time.sleep(0.01)
                self.terminate()
                break
            else:
                time.sleep(1)

    def stop(self):
        self.kill_rtlamr_thread()
        return

    def terminate(self):
        if self.t_rtlamr != None:
            if self.t_rtlamr.rtlamr_status == 'running':
                self.kill_rtlamr_thread()

        rtltcp_pid = self.get_rtltcp_pid()
        if rtltcp_pid != 0:
            os.kill(rtltcp_pid, signal.SIGKILL)
            logging.info("kill rtl_tcp pid: {}".format(rtltcp_pid))
        else:
            err_msg = "No matching rtl_tcp instance found"
            logging.error(err_msg)

        if self.t_rtltcp != None:
            self.t_rtltcp.stop()
            #self.t_rtltcp.join()
            self.t_rtltcp = None
        self.jobdone = True

    def job_done(self):
        return self.jobdone

    def get_rtlamr_pid(self):
        pid_info = get_rtlamr_pid_info()
        rtlamr_pid = 0
        if pid_info:
            for pid, value in pid_info.items():
                if value:
                    match = True
                    for k, v in value.items():
                        if k in self.rtltcp_opts:
                            if v != self.rtltcp_opts[k]:
                                match = False
                                break
                        elif k in self.rtlamr_opts:
                            if v != self.rtlamr_opts[k]:
                                match = False
                                break
                    if match:
                        rtlamr_pid = pid
                else:
                    rtlamr_pid = pid

        return rtlamr_pid

    def get_rtltcp_pid(self):
        pid_info = get_rtl_tcp_pid_info()
        rtltcp_pid = 0
        if pid_info:
            for pid, value in pid_info.items():
                if value:
                    if 'device_index' in value:
                        if value['device_index'] == self.device_index:
                            rtltcp_pid = pid
                            break

        return rtltcp_pid

    def kill_rtlamr_thread(self):
        logging.info("stop rtlamr thread")
        self.queue.put("Task finished")
        #self.queue.join()
        rtlamr_pid = self.get_rtlamr_pid()

        if rtlamr_pid != 0:
            os.kill(rtlamr_pid, signal.SIGKILL)
            logging.debug("Kill rtlamr pid: {}".format(rtlamr_pid))
        else:
            err_msg = "No matching rtlamr instance found"
            logging.error(err_msg)

        self.t_rtlamr.stop()
        #self.t_rtlamr.join()
        self.t_rtlamr = None

        return

    def get_rtlamr_status(self):
        if self.t_rtlamr != None:
            return self.t_rtlamr.rtlamr_status
        else:
            return "stopped"

    def get_rtltcp_status(self):
        if self.t_rtltcp != None:
            return self.t_rtltcp.rtltcp_status
        else:
            return "stopped"
