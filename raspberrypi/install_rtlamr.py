#!/usr/bin/env python

import sys, os
import subprocess
import commands

# Build Go 1.5 on Raspberry Pi

def install_bootstrap_go():
    # Getting the bootstrap Go 1.4 compiler
    home_dir = os.environ['HOME']
    os.chdir(home_dir)
    os.system('wget http://dave.cheney.net/paste/go-linux-arm-bootstrap-c788a8e.tbz')
    os.system('tar xjf go-linux-arm-bootstrap-c788a8e.tbz')

def fetch_go_source():
    # Fetch the Go 1.5 source
    home_dir = os.environ['HOME']
    os.chdir(home_dir)
    os.system('curl http://storage.googleapis.com/golang/go1.5.src.tar.gz | tar xz')

def set_ulimit(val):
    # Lower the default stack size from 8mb to 1mb
    cmd = "ulimit -s " + str(val)
    os.system(cmd)
    out_tuple = commands.getstatusoutput('ulimit -s')
    if out_tuple[0] == 0 and out_tuple[1] == val:
        print "Error: ulimit -s %d failed, exiting...", val
        sys.exit(2)

def build_go():
    home_dir = os.environ['HOME']
    go_src_dir = home_dir + "/go/src"
    os.chdir(go_src_dir)
    print "Build Go 1.5, it could take several hours, be patient..."
    os.system('env GO_TEST_TIMEOUT_SCALE=10 GOROOT_BOOTSTRAP=$HOME/go-linux-arm-bootstrap ./all.bash')

def add_go_env():
    home_dir = os.environ['HOME']
    fname = home_dir + "/.bashrc"
    gopath = home_dir + "/gocode"
    goroot = home_dir + "/go"
    try:
        f = open(fname, 'r+')
    except IOError:
        print "Error: can't oepn file %s for reading" % fname
        sys.exit(2)

    found = 0
    for line in f:
        if "GOPATH" in line:
            found = 1
            break

    if found == 0:
        f.write("\n")
        mystring = "export GOROOT=" + goroot + "\n"
        f.write(mystring)
        mystring = "export GOPATH=" + gopath + "\n"
        f.write(mystring)
        cwd = os.getcwd()
        mystring = "export PATH=$PATH:$GOPATH/bin:$GOROOT/bin\n"
        f.write(mystring)
        #mystring = "export PROJ_PATH=$HOME/proj/rtlamr\n"
        #f.write(mystring)
        #mystring = "export PYTHONPATH=$PYTHONPATH:$PROJ_PATH:$PROJ_PATH/webapp\n"
        #f.write(mystring)

    f.close()

    if not os.path.isdir(gopath):
        os.mkdir(gopath)
    os.environ["GOPATH"] = gopath

    mydir = home_dir + "/go-linux-arm-bootstrap"
    if os.path.isdir(mydir):
        cmd = ['rm', '-rf', mydir]
        subprocess.call(cmd)

    cmd = ["source", home_dir + "/.bashrc"]
    subprocess.call(cmd)

def build_rtlamr():
    home_dir = os.environ['HOME']
    os.chdir(home_dir)
    goroot = home_dir + "/go"
    os.system('{0}/bin/go get github.com/bemasher/rtlamr'.format(goroot))


def main():
    if subprocess.call(['which', 'go']):
        install_bootstrap_go()
        fetch_go_source()
        set_ulimit(1024)
        build_go()
        add_go_env()

    if subprocess.call(['which', 'rtlamr']):
        build_rtlamr()

if __name__ == "__main__":
    main()
