#!/usr/bin/env python

import subprocess
import Queue
import signal
import threading
import logging
import sys, os
from datetime import datetime
from utility import *

class rtltcpThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(),
            kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name,
                verbose=verbose)
        self.args = args
        self.host = args[0]
        self.port = args[1]
        self.device_index = args[2]
        self.kwargs = kwargs
        self.rtltcp_status = 'stopped'
        return


    def run(self):
        opt = ['-a', self.host, '-p', self.port, '-d', str(self.device_index)]
        logging.info(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') + ": running {} with {}".format(self.name, opt))
        self.run_rtltcp(opt)
        return


    def run_rtltcp(self, rtltcp_opt=None):
        command = ['rtl_tcp']
        if rtltcp_opt:
            command = command + rtltcp_opt

        try:
            pid = subprocess.Popen(command, stdout=subprocess.PIPE)
            self.rtltcp_status = 'running'
        except subprocess.CalledProcessError as error:
            err_msg = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') + \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + error.output
            logging.critical(err_msg)
            #sys.exit(2)

        return

    def stop(self):
        self.rtltcp_status = 'stopped'
