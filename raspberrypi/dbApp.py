#!/usr/bin/python

##############################################################################################
# Name: dbApp
# Author: Ming Liu
# Date: 02/07/2018
# Description: Class to connect to SQLite3 database and execute SQL command
##############################################################################################

import sqlite3
from sqlite3 import Error
import json
import logging

class dbApp:

    def __init__(self, db_name):
        """ create a database connection to a SQLite database """
        try:
            self.db = sqlite3.connect(db_name)
            self.curs = self.db.cursor()
        except Error as e:
            logging.error(e)

    def exe_sql(self, sql_str):
        with self.db:
            self.curs.execute(sql_str)

    def fetchone(self):
        with self.db:
            return self.curs.fetchone()

    def fetchall(self):
        with self.db:
            return self.curs.fetchall()

    def db_close(self):
        try:
            self.db.close()
        except Error as e:
            logging.warning(e)
