# UtilityApp
A Raspberry Pi (Rpi) based automated meter reading (AMR) and GPS tracking device that allows utility companies to perform drive-by meter reading and asset tracking of their utility vehicles. The AMR is achieved through low-cost software-defined-radio (SDR) receiver and GPS tracking through a USB GPS unit. The acquired meter and GPS data can be lively streamed to Google Cloud IoT Core through Helium radio channel using Helium Atom module, Rpi adapter and Element gateway.

## Motivation
Advanced (smart) metering infrastructure (AMI) installations in US rise rapidly and are expected to hit 90M in 2020. However, they are almost exclusively for electric utilities. the majority of water meters and many electric and gas utility meters remain as automated meter reading (AMR). AMR requires utility vehicle to drive by cusomer's meter installation to pick up the broadcasted wireless meter signals. Utility vehicles usually have to carry expensive OEM meter reading equipment (about $20,000). To make the matter worse, these meter reading equipments are unique to each meter OEM manaufacture and even for the same OEM, different models do not share the same protocols thus separate meter reading equipments are usually required. This project exploits the programmability of software-defined-radio receiver to receive AMR signals of different frequency and different protocols. The solution is to insert multiple low-cost SDR receivers into the USB ports of a Raspberry Pi to listen to several frequencies and protocols simultaneousely without the need to switch to different OEM meter readers. Additionally, the AMR meter data are streamed to the Google IoT and stored in Cloud SQL database. The latter can be easily integrated to cloud data analytics for driving business intelligence and it can also be integrated to utility billing system. 

Besides meter reading, most utility companies do not currently have the capability to track their assets, namely utility vehicles, while being deployed in the field. This project enables utility companies to lively track their utility vehicles to better manage their fleet and improve asset utilization efficiency.

## Hardware Components
* Raspberry Pi 3 ($40)
![Alt text](https://media.digikey.com/photos/Raspberry%20Pi/RASPBERRY-PI-3_sml.jpg)
* Helium Ethernet Starter Kit (Raspberry Pi) ($159.95)
![Alt text](https://cdn.sparkfun.com//assets/parts/1/2/6/7/6/14548-Helium_Ethernet_Starter_Kit__Raspberry_Pi_-02.jpg)
* Realtek RTL2832 based DVB dongle as SDR receiver (2 x $21.95)
![Alt text](https://images-na.ssl-images-amazon.com/images/I/41uUThVfNBL.jpg)
* Summitlink External GPS Antenna Vk-162 for DIY ADS-B Remote Mount USB GPS ($17.95)
![Alt text](https://images-na.ssl-images-amazon.com/images/I/61Oaaz7gp9L._SX425_.jpg)

## Hardware Setup

### Assemble Helium Atom, Rpi adapter and Rpi together as shown in the photo below

![Alt text](https://www.helium.com/assets/docs/raspi/raspi_shield-904dd4087e3bc0bc551286053703ae5c8ff7b92ace1c3c7642c005e0b7435185.jpg)

### Insert two Realteck RTL2832 SDR dongles and GPS unit into the USB ports of Rpi. Because of the size of the SDR dongle that does not allow multiple SDR dongels to fit the Rpi USB ports, USB extention cords are used. A complete assembled device is shown below

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/6e160fa55eb6d602f646a67e4c224c392c87a0b2/raspberrypi/images/assembled_components.jpg)

### During an actual deployment, the device can be mounted on a vehicle which powers the Rpi through the vehicle's phone charger/cigarette lighter. Here I am showing the device mounted on my car for live testing.

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/6e160fa55eb6d602f646a67e4c224c392c87a0b2/raspberrypi/images/rpi_connection_in_vehicle.jpg)
![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/6e160fa55eb6d602f646a67e4c224c392c87a0b2/raspberrypi/images/sdr_antenna_and_gps.jpg)

### Connect the Helium Ethenet Element gateway with your home router and plug in power.

## Emulate Live Data Stream

Please note that for this project to work in live data stream fashion, a deployed Helium network has to exist along the utility vehicle routes either using Helium Ethernet gateway or cellular access points. Alternatively, a Helium Cellular Access Point can be carried on utility vehicle to stream the data to Google Cloud IoT. However, due to limited budget, I opted to demonstrate the proof of concept. Instead of acquiring a more expensive Helium cellular access point, I saved the AMR meter data and GPS tracking coordinates in SQLite3 database in Rpi during live test. Then, when I got home near the Helium Element, the data were retrieved from the database using script and streamed to Google Cloud IoT to mimic the real time data streaming.

## Software Setup on Raspberry Pi
### We first do system update and upgrade

```
    sudo apt-get update
    sudo apt-get upgrade
```

### Next we install sqlite3 and associated python library. Sqlite3 database will be used to store test data in Rpi.

  Note: if it gives you "E: Unable to locate package xxxxx", rerun "sudo apt-get update", which usually fixes the problem

```
    sudo  apt-get install sqlite3 libsqlite3-0 libsqlite3-dev
    sudo pip install pysqlite
```

### Install libraries for RTL2832 based DVB dongle as SDR receiver

  To use SDR, we need to install gnuradio and rtl-sdr by issuing

```	
    python install_rtl_sdr.py
```

  *install_rtl_sdr.py* will perform the installation and configuration automatically. For details of SDR, please refere to [this link](http://osmocom.org/projects/sdr/wiki/Rtl-sdr)

  To use rtl-sdr as receiver to perform AMR meter reading, we need to build Go and install rtlamr. The necessary procedure is built into *install_rtlamr.py*. User can just simply run:

```
    python install_rtlamr.py
```

    Building Go 1.5 on Raspberry Pi is documented by [Dave Cheney](https://dave.cheney.net/2015/09/04/building-go-1-5-on-the-raspberry-pi). rtlamr go code is built from [github](https://github.com/bemasher/rtlamr).

### Set up GPS

  We first install gpsd, a daemon that receives data from a GPS receiver, and provides the data to multiple applications such as Kismet and GPS navigation software.

```
    sudo apt-get install gpsd gpsd-clients python-gps
```

  After installation, stop the gpsd.socket by issuing:

```
    sudo systemctl stop gpsd.socket
```

  Use your favarite editor to open /etc/default/gpsd and change last line to

```
    GPSD_OPTIONS="/dev/ttyACM0"
```

  Save and close the file. Then restart gpsd.

```
    sudo systemctl restart gpsd.socket
```

  To test gps is working properly, issue the following command:

```
	cgps -s
```

  You should something like the following:

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/c6c4aba72a313646c8e5acb0c1fafaa1e1b577c2/raspberrypi/images/gps_screenshot.png)

### Set up Helium on Raspberry Pi

  Install Helium Python client library

```
	sudo pip install helium-client
```

  Helium Raspberry Pi Guide can be found [here](https://www.helium.com/dev/hardware-libraries/raspberry-pi). Because we are using Raspberry Pi 3 for the project, please pay special attention to the section of "Special Configuration for Raspberry Pi 3".


  Follow the above [guide](https://www.helium.com/dev/hardware-libraries/raspberry-pi) to register the Atom prototyping module on Helium dashboard.

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/c6c4aba72a313646c8e5acb0c1fafaa1e1b577c2/raspberrypi/images/Atom_on_dashboard.png)

  Deploy the Helium Element and register it on Helium dashboard

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/c6c4aba72a313646c8e5acb0c1fafaa1e1b577c2/raspberrypi/images/Element_on_dashboard.png)

  Create Helium communication channel

* To create MQTT channel, run the following python program

```
    	from helium_client import Helium

    	helium = Helium("/dev/serial0")
    	helium.connect()

    	channel = helium.create_channel("Helium MQTT")
    	channel.send("hello from Python")
```

* To create Google IoT channel, run the following python program

```
	from helium_client import Helium

	helium = Helium("/dev/serial0")
	helium.connect()

	channel = helium.create_channel("Google IoT Core")
	channel.send("hello from Python")
```

![Alt text](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/raw/c6c4aba72a313646c8e5acb0c1fafaa1e1b577c2/raspberrypi/images/channels_on_dashboard.png)

## Operation Instruction

Smart meters transmit consumption information in the various ISM bands allowing utilities to simply send readers driving through neighborhoods to collect commodity consumption information. One protocol in particular: Encoder Receiver Transmitter (ERT) by Itron is fairly straight forward to decode and operates in the 900MHz ISM band, well within the tunable range of inexpensive rtl-sdr dongles. The underline rtlamr code is documented on [github.com/behasher/rtlamr](https://github.com/bemasher/rtlamr). 

`rtlamr` requires `rtl_tcp` tool to control and sample from rtl-sdr dongles. `rtl_tcp` comes from the package [github.com/bemasher/rtltcp](https://godoc.org/github.com/bemasher/rtltcp). `rtlamr` simply performs a DSP function to match the message preamble and decode the message according to the message type.

`rtlamr` supports the following message types:
  * __scm__: Standard Consumption Message. Simple packet that reports total consumption.
  * __scm+__: Similar to SCM, allows greater precision and longer meter ID's.
  * __idm__: Interval Data Message. Provides differential consumption data for previous 47 intervals at 5 minutes per interval.
  * __r900__: Message type used by Neptune R900 transmitters, provides total consumption and leak flags.
  * __r900bcd__: Some Neptune R900 meters report consumption as a binary-coded digits.

Since changing protocol on the fly is not supported, each rtl-sdr dongle can only support one message type during operation. To decode multiple message types, multiple rtl-sdr dongles are required. This project uses two rtl-sdr dongles to support two most popular messages types, namely scm and scm+.

To run `rtlamr` on command line, you simply start `rtl_tcp` instance on terminal A:

```
$ rtl_tcp
```

Then start `rtlamr` instance on terminal B:

```
$ rtlamr
```

To get help on how to use rtlamr, simply type `rtlamr -h`

```
$ rtlamr -h
Usage of rtlamr:
  -duration=0: time to run for, 0 for infinite, ex. 1h5m10s
  -filterid=: display only messages matching an id in a comma-separated list of ids.
  -filtertype=: display only messages matching a type in a comma-separated list of types.
  -format=plain: decoded message output format: plain, csv, json, or xml
  -msgtype=scm: message type to receive: scm, scm+, idm, r900 and r900bcd
  -samplefile=/dev/null: raw signal dump file
  -single=false: one shot execution, if used with -filterid, will wait for exactly one packet from each meter id
  -symbollength=72: symbol length in samples
  -unique=false: suppress duplicate messages from each meter
  -version=false: display build date and commit hash

rtltcp specific:
  -agcmode=false: enable/disable rtl agc
  -centerfreq=0: center frequency to receive on
  -directsampling=false: enable/disable direct sampling
  -freqcorrection=0: frequency correction in ppm
  -gainbyindex=0: set gain by index
  -offsettuning=false: enable/disable offset tuning
  -rtlxtalfreq=0: set rtl xtal frequency
  -samplerate=0: sample rate
  -server=127.0.0.1:1234: address or hostname of rtl_tcp instance
  -testmode=false: enable/disable test mode
  -tunergain=0: set tuner gain in dB
  -tunergainmode=false: enable/disable tuner gain
  -tunerxtalfreq=0: set tuner xtal frequency
```

Because this project is targeting utility vehicle application, it should not require user to issue interactive commands and it is not practical to type on keyboard and watch a screen display while driving. Thus the job should start automatically when device is powered up. Thus, a system service job is created to fulfill that need.

```
[Unit]
Description=Perform AMR meter reading and track GPS utility trucks
After=network.target

[Service]
Type=simple
User=root
WorkingDirectory=/home/pi/proj/amr_fleet_mon
ExecStart=/usr/bin/python /home/pi/proj/amr_fleet_mon/utilityApp.py
PIDFile=/var/run/utilityApp.pid
Restart=always

[Install]
WantedBy=multi-user.target
```

Additionally, to fulfill the server-client requirement of `rtl_tcp` and `rtlamr` instances as well as multiple rtl-sdr dongles to service multiple message types, a multithread scheme is adoped as explained in later section. 

Instead of command line options, option values are hard coded in the program as default. However, a json file *rtlamr_opts.config* can be created to provide custom options such as following:

```
           [
             {
                "duration": "1h5m10s",
                "filterid": ["123456", "456789"],
                "filtertype": ["11", "13"],
                "format": "json",
                "msgtype": "scm",
                "samplefile": "/dev/null",
                "single": "false",
                "server": "127.0.0.1:1234"
             },
             {
                "duration": "1h5m10s",
                "filterid": ["654321", "987654"],
                "filtertype": ["11", "13"],
                "format": "json",
                "msgtype": "scm",
                "samplefile": "/dev/null",
                "single": "false",
                "server": "127.0.0.1:1235"
             }
           ]

```

## Code Explanation

### Set Rpi system clock at powerup

  Becuase Rpi does not have a RTC to keep system clock when power is removed, I used the first available GPS signal to set up the Rpi's system clock after power up, i.e., vehicle ignition turned on. The clock setting code is in [*set_sysclk.py*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/set_sysclk.py). *set_sysclk.py* is run as a system service task as following:
  
```
[Unit]
Description=Use GPS time to set up system clock
After=network.target

[Service]
Type=oneshot
User=root
WorkingDirectory=/home/pi/proj/amr_fleet_mon
ExecStart=/usr/bin/python /home/pi/proj/amr_fleet_mon/set_sysclk.py
PIDFile=/var/run/set-sysclk.pid
Restart=no

[Install]
WantedBy=multi-user.target
```
  
### *utilityApp.py* - main

  [*utilityApp.py*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/utilityApp.py) is the entry point of the entire application. To run the program, you simply type

```
$ sudo python utilityApp.py
```

  Here is an explanation of the program
  
#### Make sure gpsd is running 

```
# Check gpsd is running. If not launch it.
start_gpsd()
```

  * Start Helium channel
    
```
try:
    helium = Helium("/dev/serial0")
    helium.connect()
    channel = helium.create_channel("Google IoT Core")
except:
    channel = None
```

  For the in-field testing, the above code would result in `channel = None` because there is no field deployed Helium network and Helium cellular access point was not carried in my car. Thus, the data were saved in sqlite3 database for emulation later.

#### Create sqlite3 database  

```
db_dir = '/var/lib/helium'
if os.path.isdir(db_dir) == False:
    os.makedirs(db_dir)

db_name = db_dir + '/gps_meters.db'
mydbApp = dbApp.dbApp(db_name)
sql_str = """CREATE TABLE IF NOT EXISTS amr (
                time DATETIME PRIMARY KEY,
                cpuserial TEXT NOT NULL,
                MeterID INTEGER NOT NULL,
                ERT_Type TINYINT,
                TamperPhy SMALLINT,
                TamperEnc SMALLINT,
                Consumption INTEGER,
                MsgType VARCHAR(8)) """

mydbApp.exe_sql(sql_str)

sql_str = """CREATE TABLE IF NOT EXISTS gps (
                time DATETIME PRIMARY KEY,
                cpuserial TEXT NOT NULL,
                lon FLOAT NOT NULL,
                lat FLOAT NOT NULL,
                alt FLOAT NOT NULL,
                speed FLOAT NOT NULL,
                climb FLOAT NOT NULL,
                track FLOAT NOT NULL,
                epx FLOAT,
                epy FLOAT,
                epv FLOAT,
                ept FLOAT,
                mode TINYINT NOT NULL,
                device TEXT,
                class TEXT) """

mydbApp.exe_sql(sql_str)
```

#### Determine how many rtl-sdr dongles are present and assign msgtype to each rtl-sdr dongle to form an *app*

```
    device_count = get_sdr_device_count()
    logging.info("device_count={}".format(device_count))
    if device_count == 0:
        logging.error("No SDR dongle present, exiting...")
        sys.exit(2)
   
    msgtypes = ['scm', 'scm+', 'idm', 'r900', 'r900bcd']
    for device_index in range(device_count):
        app = {}
        app['obj'] = None
        app['rtlamr_status'] = 'stopped'
        app['rtltcp_status'] = 'stopped'
        if opts and device_index < len(opts):
            app['rtltcp_opts'] = opts[device_index]['rtltcp_opts']
            app['rtlamr_opts'] = opts[device_index]['rtlamr_opts']

            if not 'server' in app['rtltcp_opts']:
                app['rtltcp_opts'] = {'server': '127.0.0.1:' + str(1234 + device_index)}

            if not 'msgtype' in app['rtlamr_opts']:
                app['rtlamr_opts']['msgtype'] = msgtypes[device_index]
        else:
            app['rtltcp_opts'] = {'server': '127.0.0.1:' + str(1234 + device_index)}
            app['rtlamr_opts'] = {'unique': 'true', 'msgtype': msgtypes[device_index]}


        # Force json data type
        app['rtlamr_opts']['format'] = 'json'

        apps.append(app)
```

#### Start rtlamr app for each rtl-sdr dongle by creating an instance out of AMR class

```
def app_start():
    global apps

    timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    logging.info(timestamp + ": starting meter read...")
    for device_index in range(len(apps)):
        rtltcp_opts = apps[device_index]['rtltcp_opts']
        rtlamr_opts = apps[device_index]['rtlamr_opts']
        apps[device_index]['obj'] = AMR.AMR(client_sock=channel, rtltcp_opts=rtltcp_opts, rtlamr_opts=rtlamr_opts, device_index=device_index)
        apps[device_index]['obj'].start()
        apps[device_index]['rtlamr_status'] = 'running'
        apps[device_index]['rtltcp_status'] = 'running'
```

#### Listen on port 2947 (gpsd) and save gps coordinates to sqlite3 database and stream to Google IoT if Helium network is present.

```
    # Listen on port 2947 (gpsd) of localhost
    session = gps.gps("localhost", "2947")
    session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

    time.sleep(10)
    app_start()
    cpuserial = get_serial()

    count = 0
    while True:
        try:
            report = session.next()
            # Wait for a 'TPV' report and display the current time
            # To see all report data, uncomment the line below
            # print report
            if report['class'] == 'TPV':
                mydict = report.__dict__
                gps_str = json.dumps(mydict)
                logging.debug(gps_str)
                sql_str = """INSERT OR REPLACE INTO gps (time,
                                              cpuserial,
                                              lon,
                                              lat,
                                              alt,
                                              speed,
                                              climb,
                                              track,
                                              epx,
                                              epy,
                                              epv,
                                              ept,
                                              mode,
                                              device,
                                              class)
                            VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}',
                                       '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}')"""
                mydbApp.exe_sql(sql_str.format(mydict['time'],
                                               cpuserial,
                                               mydict['lon'],
                                               mydict['lat'],
                                               mydict['alt'],
                                               mydict['speed'],
                                               mydict['climb'],
                                               mydict['track'],
                                               mydict['epx'],
                                               mydict['epy'],
                                               mydict['epv'],
                                               mydict['ept'],
                                               mydict['mode'],
                                               mydict['device'],
                                               mydict['class']))
                if channel != None:
                    if count == 0:
                        mylist = [GPS_DATATYPE, cpuserial, mydict['lat'], mydict['lon']]
                        myList = ','.join(map(str, mylist))
                        logging.debug("GPS: " + myList)
                        try:
                            channel.send(myList)
                        except Exception as e:
                            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
                            logging.error(timestamp + ': Exception ' + e.__class__.__name__ + 'happened')
                            print_exc()
                    count = count + 1
                    if count == gps_interval:
                        count = 0
        except KeyError:
            pass
        except KeyboardInterrupt:
            app_terminate()
            if channel == None and mydbApp:
                mydbApp.db_close()
            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            logging.info(timestamp + ": all done")
            quit()
        except StopIteration:
            session = None
            if channel == None and mydbApp:
                mydbApp.db_close()
            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            logging.info(timestamp + ": GPSD has terminated")  
```

### *AMR.py* - Class to launch rtl_tcp-rtlamr server-client pair

  Each AMR instance handles one pair of rtl_tcp-rtlamr server-client pair through a socket connection between the two. Both rtl_tcp and rtlamr are launched with separate threads. [*AMR*](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/AMR.py) contains following methods 

    1. `launch_rtltcp()` - start rtltcpThread that runs `rtl_tcp` to start rtl-sdr receiver
    1. `launch_rtlamr()` - start rtlamrThread that runs `rtlamr` to perform DSP function to match ERT preamble and decode ERT message
    1. `start(self)` - call `launch_rtltcp()` and `launch_rtamr()` functions
    1. `process_mon(self)` - If duration is set to a value other than 0, this function monitors the elapsed time reaches the pre-set duration then terminate the rtltcpThread and rtlamrThread.
    1. `stop(self)` - Force kill rtlamrThread but leave rtltcpThread alive.
    1. `terminate(self)` - Kill rtlamrThread first then rtltcpThread.
    1. `jobdone(self)` - return jobdone status
    1. `get_rtlamr_pid(self)` -  get pid of `rtlamr` subprocess
    1. `get_rtltcp_pid(self)` - get pid of `rtl_tcp` subprocess
    1. `kill_rtlamr_thread(self)` - kill rtlamrThread
    1. `get_rtlamr_status(self)` - return rtlamrThread status (running, stopped)
    1. `get_rtltcp_status(self)` - return rtltcpThread status (running, stopped)

### *rtltcpThread.py* - Class to launch `rtl_tcp` server subprocess thread

  [This class](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/rtltcpThread.py) will launch `rtl_tcp` subprocess.

### *rtlamrThread.py* - Class to launch `rtlamr` client subprocess thread

  [This class's](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/raspberrypi/rtlamrThread.py) `run(self)` method parses the stdout of `rtlamr` then saves the AMR meter data to sqlite3 database. If the Helium network is present, the data are also streamed through Helium channel to Google IoT. Because the program is run as a service job, there is no interactive shell. To allow program to interrupt the program, a separate thread is used to pipeline `rtlamr` stdout to a Queue for processing. The relevant code is shown below:

```
    def run(self):
        GPS_DATATYPE = 1
        METER_DATATYPE = 2
        command = ['/home/pi/gocode/bin/rtlamr']
        if self.rtltcp_opts:
            for k, v in self.rtltcp_opts.items():
                command.append('-' + k + '=' + v)
        if self.rtlamr_opts:
            for k, v in self.rtlamr_opts.items():
                command.append('-' + k + '=' + v)
        #print "command={}".format(command)

        try:
            rtlamr_pid = subprocess.Popen(command, stdout=subprocess.PIPE)
            self.rtlamr_status = 'running'
            logging.debug(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') + ": running rtlamr with {}".format(self.rtlamr_opts))
        except subprocess.CalledProcessError as error:
            err_msg = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ') \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + str(error.output)
            logging.critical(err_msg)
            #sys.exit(2)
            return 

        t = threading.Thread(target=self.enqueue_output, 
                args=(rtlamr_pid.stdout, self.out_q))
        t.setDaemon(True)
        t.start()

        db_dir = '/var/lib/helium'
        db_name = db_dir + '/gps_meters.db'
        mydbApp = dbApp.dbApp(db_name)

        while True:
            try: line = self.out_q.get_nowait()
            except Queue.Empty:
                time.sleep(0.01)
            else:
                if "Time" in line and "Consumption" in line:
                    mystr = line.rstrip('\n')
                        
                    try:
                        data = json.loads(mystr)
                    except ValueError, e:
                        logging.warning("JSON Error: {} {}".format(mystr, str(e)))
                        continue

                    self.result['DateTime'] = data["Time"]
                    if self.msgtype == 'scm+':
                        self.result['MeterID'] =  data["Message"]["EndpointID"]
                        self.result['ERT_Type'] = data["Message"]["ProtocolID"]
                        self.result['TamperPhy'] = data["Message"]["Tamper"]
                        self.result['TamperEnc'] = data["Message"]["EndpointType"]
                        self.result['Consumption'] = data["Message"]["Consumption"]
                    else:
                        self.result['MeterID'] =  data["Message"]["ID"]
                        self.result['ERT_Type'] = data["Message"]["Type"]
                        self.result['TamperPhy'] = data["Message"]["TamperPhy"]
                        self.result['TamperEnc'] = data["Message"]["TamperEnc"]
                        self.result['Consumption'] = data["Message"]["Consumption"]

                    offset = self.result['DateTime'][-6:]
                    offset_list = offset.split(':')
                    UTC_OFFSET = int(offset_list[0]) + int(offset_list[1])/60
                    localtime = parser.parse(self.result['DateTime'][:-6])
                    utctime = localtime - timedelta(hours=UTC_OFFSET)
                    sql_str = """INSERT OR REPLACE INTO amr (time, 
                                      cpuserial, 
                                      MeterID, 
                                      ERT_Type, 
                                      TamperPhy, 
                                      TamperEnc, 
                                      Consumption, 
                                      MsgType)
                               VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')"""

                    mydbApp.exe_sql(sql_str.format(utctime.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                                       self.cpuserial, 
                                       self.result['MeterID'],
                                       self.result['ERT_Type'], 
                                       self.result['TamperPhy'], 
                                       self.result['TamperEnc'], 
                                       self.result['Consumption'], 
                                       self.msgtype))

                    if self.client_sock != None:
                        #epoch_time = timegm(utctime.timetuple())
                        #mylist = [METER_DATATYPE, epoch_time, self.cpuserial, self.result['MeterID'], self.result['ERT_Type'], 
                        #          self.result['TamperPhy'], self.result['TamperEnc'], self.result['Consumption'], self.msgtype]
                        mylist = [METER_DATATYPE, self.cpuserial, self.result['MeterID'], self.result['ERT_TYPE'], self.result['Consumption'], self.msgtype] 
                        myList = ','.join(map(str, mylist))
                        logging.debug("Meter Reading: " + myList)
                        try:
                            self.client_sock.send(myList)
                        except Exception as e:
                            timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
                            logging.error(timestamp + ': Exception ' + e.__class__.__name__ + 'happened')
                            print_exc()

                elif "Time Limit Reached" in line:
                    self.out_q.task_done()
                    self.queue.task_done()
                    mydbApp.db_close()
                    break;

            try:
                msg = self.queue.get_nowait()
                if msg == "Task finished":
                    self.queue.task_done()
                    mydbApp.db_close()
                    break
            except Queue.Empty:
                continue


    def enqueue_output(self, out, queue):
        for line in iter(out.readline, ''):
            print line
            queue.put(line)
        out.close()
```
### *dbApp.py* - Class to wrap commonly used sqlite3 db functions

    1. `__init(self, db_name)__` - Class object constructor. It will connect to database specified with db_name.
    1. `exe_sql(self, sql_str)` - Execute SQL string
    1. `fetchone(self)` - fetch single line query result
    1. `fetchall(self)` - fetch multi-line query result
    1. `db_close(self)` - close db

### *amr_emulator.py* - Emulate AMR meter data streaming to Google IoT

  Due to the non-presence of Helium channel in my live car drive-by test, an emulation of AMR data stream is done through pulling saved data from sqlite3 database and stream the data through Helium channel.

```
#!/usr/bin/env python

import os, sys
import time
import string
from datetime import datetime, timedelta
from helium_client import Helium
from dateutil import parser

import dbApp

# Check gpsd is running. If not launch it.
GPS_DATATYPE = 1
METER_DATATYPE = 2

try:
    helium = Helium("/dev/serial0")
    helium.connect()
    channel = helium.create_channel("Google IoT Core")
except:
    print "Cannot open helium channel, exiting..."
    sys.exit(2)


db_dir = '/var/lib/helium'

db_name = db_dir + '/gps_meters.db'
mydbApp = dbApp.dbApp(db_name)

sql_str = """SELECT time, cpuserial, MeterID, ERT_TYPE, Consumption, MsgType from amr
             WHERE datetime(time) > datetime('2018-05-18T00:00:00Z')"""
mydbApp.exe_sql(sql_str)

rows = mydbApp.fetchall()

count = 0
for row in rows:
    timestamp = parser.parse(row[0])
    mylist = [METER_DATATYPE]
    mylist.extend(row[1:])
    myList = ','.join(map(str, mylist))
    print myList
    if count > 0:
        wait_time = (timestamp - pre_timestamp).total_seconds()
        print "Wait time={} s".format(wait_time)
        time.sleep(wait_time)
    channel.send(myList)
    pre_timestamp = timestamp
    count = count + 1

mydbApp.db_close()
```
* *gps_emulator.py* - Emulate GPS data streaming to Google IoT

  GPS emulation is done the same way as AMR data streaming emulation.
  
```
#!/usr/bin/env python

import os, sys
import time
import string
from datetime import datetime, timedelta
from helium_client import Helium
from dateutil import parser

import dbApp

# Check gpsd is running. If not launch it.
GPS_DATATYPE = 1
METER_DATATYPE = 2

try:
    helium = Helium("/dev/serial0")
    helium.connect()
    channel = helium.create_channel("Google IoT Core")
except:
    print "Cannot open helium channel, exiting..."
    sys.exit(2)


db_dir = '/var/lib/helium'

db_name = db_dir + '/gps_meters.db'
mydbApp = dbApp.dbApp(db_name)

sql_str = """SELECT time, cpuserial, lat, lon from gps
             WHERE datetime(time) > datetime('2018-05-20T00:00:00Z')"""
mydbApp.exe_sql(sql_str)

rows = mydbApp.fetchall()

count = 0
pre_timestamp = parser.parse('2018-05-20T00:00:00Z')
for row in rows:
    timestamp = parser.parse(row[0])
    time_diff = (timestamp - pre_timestamp).total_seconds()
    if time_diff >= 1:
        mylist = [GPS_DATATYPE]
        mylist.extend(row[1:])
        myList = ','.join(map(str, mylist))
        print myList
        if count > 0:
            print "Wait time={} s".format(time_diff)
            time.sleep(time_diff)
        try:
            channel.send(myList)
        except: 
            print "Connection dropped, attempt to reconnect..."
            channel = None
            time.sleep(1)
            if helium.connected() == False:
                helium.close()
                try:
                    helium = Helium("/dev/serial0")
                    helium.connect()
                    channel = helium.create_channel("Google IoT Core")
                except:
                    print "Cannot open helium channel, exiting..."
                    sys.exit(2)
            else:
                try:
                    channel = helium.create_channel("Google IoT Core")
                except:
                    print "Cannot open helium channel, exiting..."
                    sys.exit(2)
            channel.send(myList)

        pre_timestamp = timestamp
        count = count + 1

mydbApp.db_close()
```

## Google Cloud and Google Map Application
Please continue with [this link](https://bitbucket.org/mingmq6/rtlamr_bt_rpi/src/amr_fleet_mon/webapp/README.md) to check out the Google cloud and Google map application for this project.