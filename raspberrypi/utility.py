#!/usr/bin/env python

import sys, os
import subprocess
import json
import logging
import time
import gps
import dbApp

def get_serial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo','r')
        for line in f:
            if line[0:6]=='Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial


def parse_rtlamr_duration(duration_str):
    myduration = 0
    mystr = duration_str
    if mystr != '0':
        if 'h' in mystr:
            list1 = mystr.split('h')
            if list1[0].isdigit():
                hr = list1[0]
                myduration = myduration + (int(hr) * 3600)
                if 'm' in list1[1]:
                     list2 = list1[1].split('m')
                else:
                     list2 = ['0']

                if list2[0].isdigit():
                    minute = list2[0]
                    myduration = myduration + (int(minute) * 60)
                    if 's' in list2[1]:
                        list3 = list2[1].split('s')
                    else:
                        list3 = ['0']

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

        else:
            if 'm' in mystr:
                list2 = mystr.split('m')

                if list2[0].isdigit():
                    minute = list2[0]
                    myduration = myduration + (int(minute) * 60)
                    if 's' in list2[1]:
                        list3 = list2[1].split('s')
                    else:
                        list3 = ['0']

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

            else:
                if 's' in mystr:
                    list3 = mystr.split('s')

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

    return myduration


def get_sdr_device_count():
    count = 0
    command = ['lsusb']
    try:
        pid = subprocess.Popen(command, stdout=subprocess.PIPE)
        for line in iter(pid.stdout.readline, ''):
            if "RTL28" in line and "DVB-T" in line:
                count = count + 1
            elif line.rstrip('\n') == '':
                break
        pid.stdout.close()
    except subprocess.CalledProcessError as error:
        err_msg = datetime.now().strftime('%Y-%m-%d %H:%M:%S') \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + str(error.output)
        logging.critical(err_msg)

    return count

def parse_rtlamr_duration(duration_str):
    myduration = 0
    if duration_str != '0':
        if 'h' in duration_str:
            list1 = duration_str.split('h')
            if list1[0].isdigit():
                hr = list1[0]
                myduration = myduration + (int(hr) * 3600)
                if 'm' in list1[1]:
                     list2 = list1[1].split('m')
                else:
                     list2 = ['0']

                if list2[0].isdigit():
                    minute = list2[0]
                    myduration = myduration + (int(minute) * 60)
                    if 's' in list2[1]:
                        list3 = list2[1].split('s')
                    else:
                        list3 = ['0']

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

        else:
            if 'm' in duration_str:
                list2 = duration_str.split('m')

                if list2[0].isdigit():
                    minute = list2[0]
                    myduration = myduration + (int(minute) * 60)
                    if 's' in list2[1]:
                        list3 = list2[1].split('s')
                    else:
                        list3 = ['0']

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

            else:
                if 's' in duration_str:
                    list3 = duration_str.split('s')

                    if list3[0].isdigit():
                        sec = list3[0]
                        myduration = myduration + int(sec)

    return myduration


def get_rtl_tcp_pid_info():
    out_d = {}
    command = ['pgrep', '-a', 'rtl_tcp']
    try:
        pid = subprocess.Popen(command, stdout=subprocess.PIPE)
        for line in iter(pid.stdout.readline, ''):
            mystr = line.rstrip('\n')
            if mystr == '':
                break
            mylist = mystr.split()
            mypid = int(mylist[0])
            out_d[mypid] = {}
            out_d[mypid]['host'] = '127.0.0.1'
            out_d[mypid]['port'] = '1234'
            for index in range(len(mylist)):
                if mylist[index] == '-a':
                    out_d[mypid]['host'] = mylist[index + 1]
                elif mylist[index] == '-p':
                    out_d[mypid]['port'] = mylist[index + 1]
                elif mylist[index] == '-d':
                    out_d[mypid]['device_index'] = int(mylist[index + 1])
        pid.stdout.close()
    except subprocess.CalledProcessError as error:
        err_msg = datetime.now().strftime('%Y-%m-%d %H:%M:%S') \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + str(error.output)
        logging.critical(err_msg)

    return out_d


def get_rtlamr_pid_info():
    out_d = {}
    command = ['pgrep', '-a', 'rtlamr']
    try:
        pid = subprocess.Popen(command, stdout=subprocess.PIPE)
        for line in iter(pid.stdout.readline, ''):
            mystr = line.rstrip('\n')
            if mystr == '':
                break
            mylist = mystr.split()
            mypid = int(mylist[0])
            del mylist[0:2]
	    opts = {}
	    for item in mylist:
                s = item[1:]
                l = s.split('=')
                opts[l[0]] = l[1]
            out_d[mypid] = opts
        pid.stdout.close()
    except subprocess.CalledProcessError as error:
        err_msg = datetime.now().strftime('%Y-%m-%d %H:%M:%S') \
                    + ">>> Error while executing: \n"\
                    + command\
                    + "\n>>> Returned with error:\n"\
                    + str(error.output)
        logging.critical(err_msg)

    return out_d

def start_gpsd():
    # Check gpsd is running. If not launch it.
    command = ['pgrep', '-a', 'gpsd']
    try:
        pid = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = pid.communicate()
        pid.stdout.close()
        pid.stderr.close()
        if err:
            print "Error: " + err
            sys.exit(2)
    except subprocess.CalledProcessError as e:
        print "Error: " + str(e.output)
        sys.exit(2)

    lines = out.split('\n')
    mystr = lines[0]
    if mystr == '':
        command = ['sudo', 'gpsd', '/dev/ttyACM0', '-F', '/var/run/gpsd.sock']
        subprocess.call(command)
