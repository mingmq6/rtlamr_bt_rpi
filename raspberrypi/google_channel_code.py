#!/usr/bin/python

from helium_client import Helium

helium = Helium("/dev/serial0")
helium.connect()

channel = helium.create_channel("Google IoT Core")
channel.send("hello from Python")
